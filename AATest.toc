## Interface: 11305
## Title: MapPins
## Author: Isaac
## OptDeps: BugSack, !Swatter
## SavedVariables: MyAddonDB

embeds.xml
Libs\HereBeDragons\HereBeDragons-2.0.lua
Libs\HereBeDragons\HereBeDragons-Pins-2.0.lua
Libs\HereBeDragons\HereBeDragons-Migrate.lua

Core.lua