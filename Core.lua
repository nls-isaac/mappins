-- https://www.wowace.com/projects/ace3/pages/getting-started

--local ADDON = ...

MyAddon = LibStub("AceAddon-3.0"):NewAddon("MyAddon", "AceConsole-3.0", "AceEvent-3.0", "AceComm-3.0")

local HBD = LibStub("HereBeDragons-2.0")
local HBDPins = LibStub("HereBeDragons-Pins-2.0")
local HBDMigrate = LibStub("HereBeDragons-Migrate")

local options = {
    name = "MyAddon",
    handler = MyAddon,
    type = 'group',
    args = {
        msg = {
            type = 'input',
            name = 'My Message',
            desc = 'The message for my addon',
            set = 'SetMyMessage',
            get = 'GetMyMessage',
        },
        clear = {
            type = 'execute',
            func = 'ClearMarkers',
            name = 'Clear Markers',
            desc = 'Clear all markers from database',
        }
    },
}

function MyAddon:GetMyMessage(info)
    return myMessageVar
end

function MyAddon:SetMyMessage(info, input)
    myMessageVar = input
end

function MyAddon:ClearMarkers()
    -- clears all markers from database
    self.db.profile.markers = {}

    -- reload markers
    MyAddon:ReloadMarkers()

    MyAddon:Print("Cleared markers")
end

function MyAddon:ReloadMarkers()
    -- reloads and re-renders all markers from database 

    HBDPins:RemoveAllMinimapIcons("MyAddon")
    HBDPins:RemoveAllWorldMapIcons("MyAddon")

    if type(self.db.profile.markers) ~= "table" then
        MyAddon:Print("No markers initialised, init...")
        self.db.profile.markers = {}
    else
        -- display existing markers
        MyAddon:Print("Found markers")

        for i, v in pairs(self.db.profile.markers) do
            -- display markers
            print(i .. ' ' .. v.x .. ' ' .. v.y .. ' ' .. v.mapid)
            x, y, mapid = MyAddon:createMarker(v.x, v.y, v.mapid, i)
        end
    end
end

function MyAddon:ReloadMarker(dbidx, point, point_mm)
    -- reloads and re-renders specific marker from db

    -- delete icon
    HBDPins:RemoveWorldMapIcon("MyAddon", point)

    MyAddon:Print(point_mm)
    if point_mm ~= nil then
        HBDPins:RemoveMinimapIcon("MyAddon", point_mm)
    end

    -- re-create from database
    record = self.db.profile.markers[dbidx]
    x, y, mapid = MyAddon:createMarker(record.x, record.y, record.mapid, dbidx)
end

LibStub("AceConfig-3.0"):RegisterOptionsTable("MyAddon", options, {"pins", "pn"})

function MyAddon:OnInitialize()
    -- Code that you want to run when the addon is first loaded goes here.

    -- load libs
    local addon = self
    self.db = LibStub("AceDB-3.0"):New("MyAddonDB")
    self.gui = LibStub("AceGUI-3.0")
    self.libS = LibStub:GetLibrary("AceSerializer-3.0")
    self.libC = LibStub:GetLibrary("LibCompress")
    self.libCE = self.libC:GetAddonEncodeTable()

    -- init comms
    MyAddon:RegisterComm("MyAddon", function(prefix, msg)
        MyAddon:Print("Received Comm: "..msg)

        -- decode message
        local decoded = addon.libCE:Decode(msg)
        local two, message = addon.libC:Decompress(decoded)

        if(not two) then
            MyAddon:Print("Error decompressing: " .. message)
            return
        end

        -- Deserialize the decompressed data
        local success, final = addon.libS:Deserialize(two)
        if (not success) then
            MyAddon:Print("Error deserialising: " .. final)
            return
        end

        -- local newTable = {}
        -- for k,v in pairs(final.marker) do
        --     newTable[k] = v
        --     MyAddon:Print(k..":"..tostring(v))
        -- end

        MyAddon:Print("Decoded: "..final.marker.x.." "..final.marker.y.." "..final.marker.mapid.." "..final.marker.iconpath)

        -- save to database
        
        -- have no idea why this doesn't work, workaround is to reload all markers on share
        -- local new_ix = #addon.db.profile.markers + 1
        -- addon.db.profile.markers[new_ix] = {
        --     x=final.marker.x, 
        --     y=final.marker.y,
        --     mapid=final.marker.mapid,
        --     tooltip=final.marker.tooltip,
        --     size=final.marker.size,
        --     iconpath = final.marker.iconpath,
        --     isWaypoint = final.marker.isWaypoint
        -- }

        -- don't render own markers
        if final.sender ~= UnitName("Player") then
            table.insert(addon.db.profile.markers, final.marker)

            -- render marker
            MyAddon:ReloadMarkers()
            -- local record = addon.db.profile.markers[new_idx]
            -- x, y, mapid = MyAddon:createMarker(record.x, record.y, record.mapid, new_idx)
        else
            MyAddon:Print("Marker shared successfully")
        end

    end)

    -- x = tonumber(1)
    -- y = tonumber(1)
    -- mapid = -1

    -- initialise table
    MyAddon:ReloadMarkers()

    -- local defaults = {
    --     profile = {
    --       optionA = true,
    --       optionB = false,
    --       suboptions = {
    --         subOptionA = false,
    --         subOptionB = true,
    --       },
    --     }
    --   }  

    -- if player clicks on map, add an icon
    WorldMapFrame.ScrollContainer:HookScript("OnMouseDown", function (self, button)
        if button=='MiddleButton' then
            local mouse_x, mouse_y = WorldMapFrame.ScrollContainer:GetNormalizedCursorPosition()
            currentPlayerUiMapID = WorldMapFrame:GetMapID()--C_Map.GetBestMapForUnit("player")

            -- add marker to db
            local new_ix = #addon.db.profile.markers + 1

            -- TODO: apply user-set default values
            addon.db.profile.markers[new_ix] = {
                x=mouse_x, 
                y=mouse_y,
                mapid=currentPlayerUiMapID,
                tooltip="Marker",
                size=15,
                iconpath = "Interface\\Addons\\AATest\\Icons\\TF\\HordeBattleMaster.tga",
                isWaypoint = false
            }

            -- render marker
            x, y, mapid = MyAddon:createMarker(addon.db.profile.markers[new_ix].x, addon.db.profile.markers[new_ix].y, addon.db.profile.markers[new_ix].mapid, new_ix)
            --table.insert(addon.db.profile.markers, )
        end
    end)

    WorldMapFrame:HookScript("OnHide", function()
         --MyAddon:Print("Hide map")
    end)

    WorldMapFrame:HookScript("OnShow", function()
         --MyAddon:Print("Show map")
    end)

    --local pos = HBD:GetUnitWorldPosition('player')
    --MyAddon:Print("Pos:"..pos.x)
end

function MyAddon:OnEnable()
    -- Called when the addon is enabled
end

function MyAddon:OnDisable()
    -- Called when the addon is disabled
end

-- function MyAddon:CreateMinimapMarker(x, y, mapid)
--     HBDPins:RemoveAllMinimapIcons("MyAddon")

--     local point = CreateFrame("Frame", nil, UIParent)
--     point:SetWidth(24)
--     point:SetHeight(24)
--     point:SetBackdrop({bgFile = "Interface\\Addons\\TownsfolkTracker\\Icons\\HordeBattleMaster.tga"})
--     HBDPins:AddMinimapIconMap("MyAddon", point, tonumber(mapid), tonumber(x), tonumber(y), true, true)
-- end

function MyAddon:TipSee()
    --if not self:IsEnabled() then return end
    GameTooltip:SetOwner(self, "ANCHOR_NONE")
    local parent = self:GetParent()
    local pscale = parent:GetEffectiveScale()
    local gscale = UIParent:GetEffectiveScale()
    local tscale = GameTooltip:GetEffectiveScale()
    local gap = ((UIParent:GetRight() * gscale) - (parent:GetRight() * pscale))
    if gap < (250 * tscale) then
        GameTooltip:SetPoint("TOPRIGHT", parent, "TOPLEFT", 0, 0)
    else
        GameTooltip:SetPoint("TOPLEFT", parent, "TOPRIGHT", 0, 0)
    end
    MyAddon:Print("Tooltip opening... " .. self.tiptext)
    GameTooltip:SetText(self.tiptext, nil, nil, nil, nil, true)
end

function MyAddon:deleteMarker(dbidx, point, point_mm)
    -- delete marker from db
    table.remove(addon.db.profile.markers, dbidx)

    -- delete icon
    HBDPins:RemoveWorldMapIcon("MyAddon", point)

    if point_mm ~= nil then
        HBDPins:RemoveMinimapIcon("MyAddon", point_mm)
    end
end

-- function MyAddon:updateMarkerTooltip(dbidx, point, text)
--     point.tiptext = text
-- end

function MyAddon:createMarker(x, y, mapid, dbidx)
    
    addon = self

    --local player_x, player_y, currentPlayerUiMapID, currentPlayerUiMapType = HBD:GetPlayerZonePosition()
    --local ping_x, ping_y = HBD:GetZoneCoordinatesFromWorld(x, y, currentPlayerUiMapID, true)
    --local target_x_w, target_y_w, instance = HBD:GetUnitWorldPosition('mouseover')

    MyAddon:Print("Marked ["..x..", "..y.."]")--..", "..ping_x..", "..ping_y)

    local point = CreateFrame("Frame", nil, UIParent)
    point:SetWidth(addon.db.profile.markers[dbidx].size)
    point:SetHeight(addon.db.profile.markers[dbidx].size)
    --point:SetBackdropColor(1,1,1);
    point:SetBackdrop({bgFile = addon.db.profile.markers[dbidx].iconpath})

    MyAddon:Print(""..dbidx)
    point.tiptext = addon.db.profile.markers[dbidx].tooltip
    point:SetScript("OnEnter", MyAddon.TipSee)
    point:SetScript("OnLeave", GameTooltip_Hide)

    -- create a minimap icon if this is a waypoint
    local point_mm = nil
    if addon.db.profile.markers[dbidx].isWaypoint then
        point_mm = CreateFrame("Frame", nil, UIParent)
        point_mm:SetWidth(addon.db.profile.markers[dbidx].size)
        point_mm:SetHeight(addon.db.profile.markers[dbidx].size)
        point_mm:SetBackdrop({bgFile = addon.db.profile.markers[dbidx].iconpath})

        -- add icon to minimap
        HBDPins:AddMinimapIconMap("MyAddon", point_mm, tonumber(mapid), tonumber(x), tonumber(y), true, true)
    end

    -- handle deletion
    point:HookScript("OnMouseDown", function (self, button)
        if button=='MiddleButton' then
            if IsShiftKeyDown() then
                -- MMB + Shift: delete marker
                MyAddon:deleteMarker(dbidx, point, point_mm)
            else
                -- MMB: toggle waypoint
                addon.db.profile.markers[dbidx].isWaypoint = not addon.db.profile.markers[dbidx].isWaypoint
                MyAddon:ReloadMarker(dbidx, point, point_mm)
            end
        elseif button=='LeftButton' then
            -- show point config menu

            -- store temporary updated values
            local tooltipText = point.tiptext
            local markerWorldSize = addon.db.profile.markers[dbidx].size
            local iconPath = addon.db.profile.markers[dbidx].iconpath
            local isWaypoint = addon.db.profile.markers[dbidx].isWaypoint

            -- Create a container frame
            local f = addon.gui:Create("Frame")
            f:SetCallback("OnClose", function(widget) addon.gui:Release(widget) end)
            f:SetTitle("Pin Config")
            f:SetStatusText("Position: "..x..", "..y..", Map: "..mapid)
            f:SetLayout("Flow")

            -- create a preview icon
            local icoMarker = addon.gui:Create("Icon")
            icoMarker:SetImageSize(markerWorldSize, markerWorldSize)
            icoMarker:SetImage(addon.db.profile.markers[dbidx].iconpath)
            icoMarker:SetLabel("Icon")
            f:AddChild(icoMarker)

            -- create share button
            local btnShare = addon.gui:Create("Button")
            btnShare:SetWidth(170)
            btnShare:SetText("Share to Party")
            btnShare:SetCallback("OnClick", function()
                -- serialise data
                local data = {
                    sender=UnitName("Player"),
                    marker=addon.db.profile.markers[dbidx]
                }
                data = MyAddon.libS:Serialize(data)
                data = MyAddon.libC:CompressHuffman(data) 
                data = MyAddon.libCE:Encode(data)

                MyAddon:SendCommMessage("MyAddon", data, "PARTY", nil, "BULK")
            end)
            f:AddChild(btnShare)

            -- create delete button
            local btnDelete = addon.gui:Create("Button")
            btnDelete:SetWidth(170)
            btnDelete:SetText("Delete")
            btnDelete:SetCallback("OnClick", function()
                MyAddon:deleteMarker(dbidx, point, point_mm)
                addon.gui:Release(f)
            end)
            f:AddChild(btnDelete)

            -- create save button
            local btnSave = addon.gui:Create("Button")
            btnSave:SetWidth(170)
            btnSave:SetText("Save")
            btnSave:SetCallback("OnClick", function()
                -- save all values to the database
                addon.db.profile.markers[dbidx].tooltip = tooltipText
                addon.db.profile.markers[dbidx].size = markerWorldSize
                addon.db.profile.markers[dbidx].iconpath = iconPath
                addon.db.profile.markers[dbidx].isWaypoint = isWaypoint

                -- reload the marker
                MyAddon:ReloadMarker(dbidx, point, point_mm)

                -- close the window
                addon.gui:Release(f)
            end)
            f:AddChild(btnSave)

            -- create tooltip edit box
            local txtTooltip = addon.gui:Create("EditBox")
            txtTooltip:SetText(tooltipText)
            txtTooltip:SetLabel("Label")
            txtTooltip:SetCallback("OnEnterPressed", function(text)
                MyAddon:Print("Set tooltip text to: " .. txtTooltip:GetText())
                --addon.db.profile.markers[dbidx].tooltip = txtTooltip:GetText()
                tooltipText = txtTooltip:GetText()
            end)
            f:AddChild(txtTooltip)

            -- create marker scale control
            local sldMarkerWorldSize = addon.gui:Create("Slider")
            sldMarkerWorldSize:SetLabel("World Marker Size")
            sldMarkerWorldSize:SetValue(markerWorldSize)
            sldMarkerWorldSize:SetSliderValues(5, 45, 1)
            sldMarkerWorldSize:SetCallback("OnValueChanged", function()
                markerWorldSize = sldMarkerWorldSize:GetValue()
                icoMarker:SetImageSize(markerWorldSize, markerWorldSize)

                --local val = sldMarkerWorldSize:GetValue()
                --MyAddon:Print("Size set to: " .. val)
                --addon.db.profile.markers[dbidx].size = val
            end)

            f:AddChild(sldMarkerWorldSize)

            -- create waypoint checkbox
            local chkWaypoint = addon.gui:Create("CheckBox")
            chkWaypoint:SetLabel("Waypoint")
            chkWaypoint:SetValue(isWaypoint)
            chkWaypoint:SetCallback("OnValueChanged", function()
                isWaypoint = chkWaypoint:GetValue()
            end)
            f:AddChild(chkWaypoint)

            -- create icon select widget
            local scrollcontainer = addon.gui:Create("InlineGroup") -- "InlineGroup" is also good
            --scrollcontainer:SetFullWidth(true)
            --scrollcontainer:SetFullHeight(true) -- probably?
            scrollcontainer:SetRelativeWidth(0.5)
            scrollcontainer:SetLayout("Fill") -- important!
            f:AddChild(scrollcontainer)

            scroll = addon.gui:Create("ScrollFrame")
            scroll:SetLayout("Flow") -- probably?
            scrollcontainer:AddChild(scroll)

            -- add icons
            local iconRoot = "Interface\\Addons\\AATest\\Icons\\TF\\"

            for i, v in pairs(PN_ICONS) do
                local icoPreview = addon.gui:Create("Icon")
                icoPreview.iconpath = iconRoot .. v
                --MyAddon:Print("Icon: " .. icoPreview.iconpath)
                icoPreview:SetImageSize(24, 24)
                icoPreview:SetImage(icoPreview.iconpath)
                icoPreview:SetWidth(24+2)

                icoPreview:SetCallback("OnClick", function()
                    -- user selected this icon
                    icoMarker:SetImage(icoPreview.iconpath)
                    iconPath = icoPreview.iconpath
                end)

                scroll:AddChild(icoPreview) 
            end

        end
    end)

    -- add marker to world map
    HBDPins:AddWorldMapIconMap("MyAddon", point, tonumber(mapid), tonumber(x), tonumber(y), HBD_PINS_WORLDMAP_SHOW_WORLD)

    return tonumber(x), tonumber(y), tonumber(mapid)
end

-- function MyAddon:WORLD_MAP_UPDATE()
--     MyAddon:Print("MAP")
-- end

-- function MyAddon:PLAYER_REGEN_DISABLED()
--     -- process minimap ping

--     HBDPins:RemoveAllMinimapIcons("MyAddon")

--     MyAddon:Print("Under attack")

--     local player_x, player_y, currentPlayerUiMapID, currentPlayerUiMapType = HBD:GetPlayerZonePosition()

--     local point = CreateFrame("Frame", nil, UIParent)
--     point:SetWidth(24)
--     point:SetHeight(24)
--     point:SetBackdrop({bgFile = "Interface\\Addons\\TownsfolkTracker\\Icons\\HordeBattleMaster.tga"})
--     currentPlayerUiMapID = C_Map.GetBestMapForUnit("player")

--     HBDPins:AddMinimapIconMap("MyAddon", point, tonumber(currentPlayerUiMapID), tonumber(player_x), tonumber(player_y), true, true)
--     --HBDPins:AddMinimapIconMap("MyAddon", point, tonumber(currentPlayerUiMapID), tonumber(player_x+(ping_x/100)), tonumber(player_y+(ping_y/100)), true, true)
--     --HBDPins:AddMinimapIconWorld("MyAddon", point, tonumber(currentPlayerUiMapID), tonumber(player_x), tonumber(player_y), true)

--     --HBDPins:AddWorldMapIconMap("MyAddon", point, tonumber(currentPlayerUiMapID), tonumber(player_x+(ping_x/100)), tonumber(player_y+(ping_y/100)), HBD_PINS_WORLDMAP_SHOW_PARENT)
-- end

--MyAddon:RegisterEvent("MINIMAP_PING")
--MyAddon:RegisterEvent("PLAYER_REGEN_DISABLED")
MyAddon:RegisterChatCommand("mark", "GLOBAL_MOUSE_DOWN")
--MyAddon:RegisterEvent("WORLD_MAP_UPDATE")
