+---------------+
|Interface Icons|
+---------------+
 by Dark Imakuni
 ---------------


This is a pack of Icons I made for use in World of WarCraft. At the moment, there are currently 54 Icons available for use in this pack. If you wish to send me some files to add to this pack to share with everyone, please do. I will add them to this pack as soon as I get them and add credit and thanks as necessary.

Please refer to the "Currently Available" section below the Patch Notes to check which Icons are included in this pack. This will ensure any Icons you had installed prior to this pack with the same name don't get over-written.

Please send all potential Icon files to imakuni2005-icon@yahoo.co.uk with "Interface Icons" as the subject. Also, please include your ui.worldofwar.net User Name so I can add you to the Special Thanks section below.


PLEASE NOTE: The Icons will only appear in World of WarCraft if the current version is v1.7.0 or above (including Test Realms).
PLEASE NOTE: At the moment, the Icons will only appear in the WoW v1.7 Test Realms. They will appear in the normal client realms once the v1.7 patch has been released. To make the Icons appear in the Test Realms, extract them to "World of WarCraft/WoWTest/Interface/Icons".


Installation
------------
To install this pack, extract all files (except this ReadMe - it won't add anything to the game) to your "World of WarCraft/Interface/Icons" folder. The Icons should then be avaiable for use to add Icons to your player-made macro's in World of WarCraft. The Icons will not be available for use if you place them elsewhere.

If the "Icons" folder does not exist in your directory, go to your "World of WarCraft/Interface" folder and create a new folder named "Icons". Then extract the Icons into that folder.


Un-installation
---------------
If you don't want to use these Icons, simply delete the ones you don't want to use from your "World of WarCraft/Interface/Icons" folder.


Special Thanks
--------------
Mr Atomos	- For submitting useful information about WinMPQ.


Patch Notes
-----------
5 September 2005
- Added 7 new Icons
- Removed version numbering. As there's no code involved in this add-on, I felt version numbers were un-necessary.

1 September 2005
- Added 21 new Icons to the collection.

25 August 2005
- Initial release.


Currently Available (all are 32x32 unless marked otherwise)
-------------------
- 26 letters of the English Alphabet (A.tga through to Z.tga)
- Acid Skull.tga
- Aqua Form.tga
- Bear Form.tga
- Blue Necklace.tga
- Cat Form.tga
- Darkmoon Card - Beast.tga
- Darkmoon Card - Blue Dragon.tga
- Darkmoon Card - Elemental.tga
- Darkmoon Card - Heroism.tga
- Darkmoon Card - Maelstrom.tga
- Darkmoon Card - Portal.tga
- Darkmoon Card - Twisting Nether.tga
- Darkmoon Card - Warlord.tga
- Darkmoon Deck.tga
- Darkmoon Necklace.tga
- Darkmoon Ring.tga
- Dice.tga
- Dragon Head.tga
- Emerald.tga
- Eternium.tga
- Lament.tga
- Mooncloth.tga
- Parchment.tga
- Question.tga
- Shadow Egg.tga
- Talisman.tga
- Topaz.tga
- Travel Form.tga